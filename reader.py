import zlib
import math

class SB:
    def read(data, len):
        d = sum([(1 if data[1:len][x] else 0) * 2**(len-2-x) for x in range(len-1)]) - ((1 << (len - 1)) if data[0] else 0)
        return [d, data[len:]]

class UB:
    def read(data, len):
        d = sum([(1 if data[:len][x] else 0) * 2**(len-1-x) for x in range(len)])
        return [d, data[len:]]

class UI8:
    def read(data):
        res, data = UB.read(data, 8)
        return [res, data]

class UI16:
    def read(data):
        val, data = UI8.read(data)
        val2, data = UI8.read(data)
        return [val2 * 256 + val, data]

class UI32:
    def read(data):
        val, data = UI8.read(data)
        val2, data = UI8.read(data)
        val3, data = UI8.read(data)
        val4, data = UI8.read(data)
        return [val4 * 16777216 + val3 * 65536 + val2 * 256 + val, data]

class SI8:
    def read(data):
        res, data = SI.read(data, 8)
        return [res, data]

class SI32:
    def read(data):
        val, data = SI8.read(data)
        val2, data = UI8.read(data)
        val3, data = UI8.read(data)
        val4, data = UI8.read(data)
        return [val4 * 16777216 + val3 * 65536 + val2 * 256 + val, data]

class RECT:
    def read(data):
        Nbits, data = UB.read(data, 5)
        Xmin, data = SB.read(data, Nbits)
        Xmax, data = SB.read(data, Nbits)
        Ymin, data = SB.read(data, Nbits)
        Ymax, data = SB.read(data, Nbits)
        return [[Xmin, Ymin, Xmax, Ymax], data]

class FILE_HEADER:
    def read(data):
        s1, data = UI8.read(data)
        s2, data = UI8.read(data)
        s3, data = UI8.read(data)
        ver, data = UI8.read(data)
        flen, data = UI32.read(data)

        out = {
            "Signature": bytes([s1, s2, s3]),
            "SWF Version": ver,
            "File length": flen,
        }
        if out["Signature"] != b"FWS":
            return [out, data]
		
        size, data = RECT.read(data)

        rawfps1, data = UI8.read(data)
        rawfps2, data = UI8.read(data)
        try:
            fps = rawfps1 + rawfps2 / int(math.ceil(math.log10(rawfps2)) + 1)
        except:
            fps = rawfps1

        fc, data = UI16.read(data)

        out.update({
            "Frame rate": fps,
            "Frame count": fc,
            "Frame size": size
        })
		
        return [out, data]

class STRING:
    def read(data):
        x = [None, ]
        while x[-1] != 0:
            res, data = UI8.read(data)
            print(res)
            x.append(res)
        return [bytes(x[1:-1]).decode("UTF-8"), data]

class RGB:
    def read(data):
        r, data = UI8.read(data)
        g, data = UI8.read(data)
        b, data = UI8.read(data)
        
        return [[r, g, b, 255], data]

class RGBA:
    def read(data):
        r, data = UI8.read(data)
        g, data = UI8.read(data)
        b, data = UI8.read(data)
        a, data = UI8.read(data)
        
        return [[r, g, b, a], data]

class ARGB:
    def read(data):
        a, data = UI8.read(data)
        r, data = UI8.read(data)
        g, data = UI8.read(data)
        b, data = UI8.read(data)
        
        return [[r, g, b, a], data]

class FIXED:
    def read(data, len):
        x, data = SI32.read(data)
        while len(x) < 32:
            x = x[0] + [False, ] + x[1:]
        before_point = x[0:16]
        after_point  = x[16:32]
        try:
            result = before_point + after_point / int(math.ceil(math.log10(after_point)) + 1)
        except:
            result = before_point
        return [result, data]

class FB:
    def read(data, len):
        x, data = SB.read(data, len)
        while len(x) < 32:
            x = x[0] + [False, ] + x[1:]
        before_point = x[0:16]
        after_point  = x[16:32]
        try:
            result = before_point + after_point / int(math.ceil(math.log10(after_point)) + 1)
        except:
            result = before_point
        return [result, data]

class MATRIX:
    def read(data):
        HasScale, data = UB.read(data, 1)
        if HasScale:
            NScaleBits, data = UB.read(data, 5)
            ScaleX, data = FB.read(data, NScaleBits)
            ScaleY, data = FB.read(data, NScaleBits)
        else:
            ScaleX = 1
            ScaleY = 1
        HasRotate, data = UB.read(data, 1)
        if HasRotate:
            NRotateBits, data = UB.read(data, 5)
            RotateSkew0, data = FB.read(data, NRotateBits)
            RotateSkew1, data = FB.read(data, NRotateBits)
        else:
            RotateSkew0 = 0
            RotateSkew1 = 0
        NTranslateBits, data = UB.read(data, 5)
        TranslateX, data = SB.read(data, NTranslateBits)
        TranslateY, data = SB.read(data, NTranslateBits)
        return [{
            "RotateSkew0": RotateSkew0,
            "RotateSkew1": RotateSkew1,
            "TranslateX": TranslateX,
            "TranslateY": TranslateY,
            "ScaleX": ScaleX,
            "ScaleY": ScaleY,
        }, data]

class CXFORM:
    def read(data):
        HasAddTerms, data = UB.read(data, 1)
        HasMultTerms, data = UB.read(data, 1)
        Nbits, data = UB.read(data, 4)
        if HasMultTerms:
            RedMultTerm, data = SB.read(data, Nbits)
            GreenMultTerm, data = SB.read(data, Nbits)
            BlueMultTerm, data = SB.read(data, Nbits)
        else:
            RedMultTerm = 256
            GreenMultTerm = 256
            BlueMultTerm = 256
        if HasAddTerms:
            RedAddTerm, data = SB.read(data, Nbits)
            GreenAddTerm, data = SB.read(data, Nbits)
            BlueAddTerm, data = SB.read(data, Nbits)
        else:
            RedAddTerm = 0
            GreenAddTerm = 0
            BlueAddTerm = 0
        return [{
            "RedAddTerm": RedAddTerm,
            "GreenAddTerm": GreenAddTerm,
            "BlueAddTerm": BlueAddTerm,
            "RedMultTerm": RedMultTerm,
            "GreenMultTerm": GreenMultTerm,
            "BlueMultTerm": BlueMultTerm,
        }, data]

class CXFORMWITHALPHA:
    def read(data):
        HasAddTerms, data = UB.read(data, 1)
        HasMultTerms, data = UB.read(data, 1)
        Nbits, data = UB.read(data, 4)
        if HasMultTerms:
            RedMultTerm, data = SB.read(data, Nbits)
            GreenMultTerm, data = SB.read(data, Nbits)
            BlueMultTerm, data = SB.read(data, Nbits)
            AlphaMultTerm, data = SB.read(data, Nbits)
        else:
            RedMultTerm = 256
            GreenMultTerm = 256
            BlueMultTerm = 256
            AlphaMultTerm = 256
        if HasAddTerms:
            RedAddTerm, data = SB.read(data, Nbits)
            GreenAddTerm, data = SB.read(data, Nbits)
            BlueAddTerm, data = SB.read(data, Nbits)
            AlphaAddTerm, data = SB.read(data, Nbits)
        else:
            RedAddTerm = 0
            GreenAddTerm = 0
            BlueAddTerm = 0
            AlphaAddTerm = 0
        return [{
            "RedAddTerm": RedAddTerm,
            "GreenAddTerm": GreenAddTerm,
            "BlueAddTerm": BlueAddTerm,
            "AlphaAddTerm": AlphaAddTerm,
            "RedMultTerm": RedMultTerm,
            "GreenMultTerm": GreenMultTerm,
            "BlueMultTerm": BlueMultTerm,
            "AlphaMultTerm": AlphaMultTerm,
        }, data]

class PlaceObjectTag:
    def read(data, length):
        original_len = len(data)
        loss = 0
        CharacterId, data = UI16.read(data)
        loss += original_len - len(data)
        Depth, data = UI16.read(data)
        loss += original_len - len(data)
        Matrix, data = MATRIX.read(data)
        loss += original_len - len(data)
        ColorTransform = None
        if loss < len:
            ColorTransform, data = CXFORM.read(data)
        return [{
            "CharacterId": CharacterId,
            "Depth": Depth,
            "Matrix": Matrix,
            "ColorTransform": ColorTransform
        }, data]

class RECORDHEADER:
    def read(data):
        LONG_TAGS = []
        TagCode, data = UB.read(data, 10)
        TagLength, data = UB.read(data, 6)
        if TagCode in LONG_TAGS:
            Length, data = UI32.read(data)
        else:
            Length = None
        return [{
            "TagCode": TagCode,
            "TagLength": TagLength,
            "Length": Length
        }, data]

TAGS = {
    4: PlaceObjectTag,
}

class Parser():
    def __init__(self, file):
        x = open(file, "rb")
        y = x.read()
        x.close()
        self.filename = file
        self.data = []
        self.raw_data = y
        for i in y:
            for i2 in bin(i)[2:].zfill(8):
                if i2 == "1":
                    self.data.append(True)
                else:
                    self.data.append(False)
        self.file_info = None
        self.tags = None
    def parse(self):
        if self.file_info:
            raise Exception("File already parsed!")
        self.file_info, self.data = FILE_HEADER.read(self.data)
    def print_data(self):
        results = self.file_info
        if results == None:
            raise Exception("File not parsed yet, call parse() first!")
        print("File %s" % (self.filename))
        print("  %s (Signature: %s)" % ("Compressed file" if results["Signature"] != b"FWS" else "Uncompressed file", results["Signature"]))
        print("  SWF Version: %s" % results["SWF Version"])
        print("  Decompressed file size: %s" % results["File length"])
        if results["Signature"] == b"FWS":
            print("  Frame rate: %s" % results["Frame rate"])
            print("  Frame count: %s" % results["Frame count"])
            print("  Screen size: %spx by %spx" % (results["Frame size"][2] / 20, results["Frame size"][3] / 20))
        else:
            print("More data not avaiable before decompression.")

def decompress_SWF(filein, fileout):
    x = Parser(filein)
    tmp, _ = FILE_HEADER.read(x.data)
    if tmp["Signature"] == b"CWS":
        y = open(fileout, "wb")
        y.write(b"F" + x.raw_data[1:8] + zlib.decompress(x.raw_data[8:]))
        y.close()
    elif tmp["Signature"] == b"ZWS":
        raise NotImplementedError("LZMA compression not supported.")
    elif tmp["Signature"] == b"FWS":
        raise ValueError("File already decompressed.")
    else:
        raise ValueError("Invalid or corrupted SWF file.")
